module "mywin-01" {
  source = "./modules/winvm"

  # hostname can not contain _
  hostname         = "mywin-01"
  domain           = "ignw.io"
  node_num_cpus    = 2
  node_memory      = 8192
  root_volume_size = 100
  disk_template    = "server-templates/windows-2016-template"
}