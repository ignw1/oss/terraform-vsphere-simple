# output "node_name" {
#   value = "${vsphere_virtual_machine.node.hostname}"
# }

output "public_ip" {
  value = "${vsphere_virtual_machine.node.default_ip_address}"
}
