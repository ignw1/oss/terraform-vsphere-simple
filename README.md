# terraform-vsphere-simple
A Terraform Module for to create a virtual machine on vSphere

Indavidual, Linux Usage:
````
terraform init
terraform apply -var-file pod1_centos.tfvars
````

Traditional folder based, Windows VM Usage:
````
terraform init
terraform apply
````
